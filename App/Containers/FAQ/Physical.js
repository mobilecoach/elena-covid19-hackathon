
// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Linking } from 'react-native'
import ParsedText from 'react-native-parsed-text'
import Collapsible from 'react-native-collapsible'
import propTypes from 'prop-types'
import {Icon, Card} from 'react-native-elements'

// import { NavigationActions } from 'react-navigation'
import {Colors} from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

const questions = []

for (let i = 1; i <= 37; i++) {
  questions.push({
    title: `Faq-PA.questions.${i}.title`,
    answer: `Faq-PA.questions.${i}.answer`
  })
}

class CollapsibleView extends Component {
  static propTypes = {
    title: propTypes.string
  }
  state = {
    collapsed: true
  }

  render () {
    const {title} = this.props
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({collapsed: !this.state.collapsed})} style={{flexDirection: 'row', alignItems: 'flex-start', marginRight: 15}}>
          <Icon name={this.state.collapsed ? 'chevron-right' : 'chevron-down'} type='font-awesome' containerStyle={{marginTop: 3, width: 15, marginRight: 5}} iconStyle={[styles.headline, {fontSize: 14}]} />
          <Text style={[styles.headline, {marginRight: 15}]}>{title}</Text>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    )
  }
}

class Physical extends Component {
  state = {
    activeSection: false,
    collapsed: true
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  setSection = section => {
    this.setState({ activeSection: section })
  }

  renderNavigationbar (props) {
    let title = I18n.t('Faq-PA.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
          <Card
            title={I18n.t('Faq-PA.faqTitle')}
            titleStyle={styles.cardTitle}
            containerStyle={{marginBottom: 15}}
            >
            <View key={1}>
              {
                questions.map((question, i) =>
                  <CollapsibleView key={i} title={(i + 1) + '. ' + I18n.t(question.title)}>
                    <View style={{ flex: 1 }}>
                      <ParsedText
                        style={styles.paragraph}
                        parse={[{type: 'email', style: styles.url, onPress: (mail) => openURL('mailto:' + mail)},
                        {type: 'url', style: styles.url, onPress: (url) => openURL(url)}]}
                      >
                        {I18n.t(question.answer)}
                      </ParsedText>
                    </View>
                  </CollapsibleView>
                )
              }
            </View>
            <CollapsibleView title={I18n.t('Faq-PA.questions.38.title')}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov//3920711/')}>
                  <Text style={{color: 'blue'}}>
                  [1]  C. J. Caspersen, K. E. Powell, and G. M. Christenson, “Physical activity, exercise, and physical fitness: definitions and distinctions for health-related research,” Public Health Reports, vol. 100, no. 2, pp. 126–131, 1985.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/832868/uk-chief-medical-officers-physical-activity-guidelines.pdf')}>
                  <Text style={{color: 'blue'}}>
                  [2]  Department of Health and Social Care, UK Chief Medical Officers’ Physical Activity Guidelines. London, UK: Department of Health and Social Care, 2019.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/health-topics/physical-activity')}>
                  <Text style={{color: 'blue'}}>
                  [3]  World Health Organization, “Physical Activity” World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://bmcpublichealth.biomedcentral.com/articles/10.1186/1471-2458-13-813')}>
                  <Text style={{color: 'blue'}}>
                  [4]  M. Reiner, C. Niermann, D Jekauc, and A. Woll, “Long-term health benefits of physical activity – a systematic review of longitudinal studies,” BMC Public Health, vol. 13, article no. 813, 2013.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://psycnet.apa.org/record/2003-01659-007')}>
                  <Text style={{color: 'blue'}}>
                  [5]  J. Salmon, N. Owen, D. Crawford, A. Bauman, and J. F. Sallis, “Physical activity and sedentary behavior: a population-based study of barriers, enjoyment, and preference,” Health Psychology, Vol. 22, no. 2, Mar pp. 178-188, 2003.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/ncds/prevention/physical-activity/global-action-plan-2018-2030/en/')}>
                  <Text style={{color: 'blue'}}>
                  [6]  World Health Organization, “Global action plan on physical activity 2018–2030: more active people for a healthier world”. Geneva, Switzerland: World Health Organization, 2018.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/pii/S2214109X20302114')}>
                  <Text style={{color: 'blue'}}>
                  [7]  T. Strain, S. Brage, S. J. Sharp, J. Richards, M. Tainio, M. Ding Ding, J. Benichou, and P. Kelly, “Use of the prevented fraction for the population to determine deaths averted by existing prevalence of physical activity: a descriptive study” The Lancet Global Health, vol. 8, no. 7, Jul pp. e920-e930, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ahajournals.org/doi/full/10.1161/CIRCOUTCOMES.118.005263')}>
                  <Text style={{color: 'blue'}}>
                  [8]  U.S. Department of Health and Human Services, Physical Activity Guidelines for Americans 2nd Edition. U.S. Department of Health and Human Services, 2018.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.dovepress.com/replacing-sedentary-time-with-physical-activity-a-15-year-follow-up-of-peer-reviewed-article-CLEP')}>
                  <Text style={{color: 'blue'}}>
                  [9]  I. M. Dohrn, L. Kwak, P. Oja, M. Sjöström, and M. Hagströmer, “Replacing sedentary time with physical activity: a 15-year follow-up of mortality in a national cohort,” Clinical Epidemiology. Vol. 10, pp. 179-186.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://ijbnpa.biomedcentral.com/articles/10.1186/s12966-015-0280-7')}>
                  <Text style={{color: 'blue'}}>
                  [10]  E. Stamatakis, K. Rogers, D. Ding, et al, “All-cause mortality effects of replacing sedentary time with physical activity and sleeping using an isotemporal substitution model: a prospective study of 201,129 mid-aged and older adults,” International  Journal of Behavioural Nutrition and Physical  Activity, vol. 12, Sep p. 121, 2015.
                  </Text>
                </TouchableOpacity>                
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://bmcpublichealth.biomedcentral.com/articles/10.1186/s12889-017-4859-6')}>
                  <Text style={{color: 'blue'}}>
                  [11]  M. Tremblay et al., “Canadian 24-Hour Movement Guidelines: An Integration of Physical Activity, Sedentary Behaviour, and Sleep,” BMC Public Health, vol. 20, no 17(Suppl 5), Nov p. 874, 2017.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www1.health.gov.au/internet/main/publishing.nsf/Content/health-pubhlth-strateg-phys-act-guidelines')}>
                  <Text style={{color: 'blue'}}>
                  [12]  The Department of Health, Australian 24-Hour Movement Guidelines for the Early Years (Birth to 5 years): An Integration of Physical Activity, Sedentary Behaviour, and Sleep. Canberra, Australia: Australian Government, 2017.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/dietphysicalactivity/factsheet_young_people/en/')}>
                  <Text style={{color: 'blue'}}>
                  [13]  World Health Organization, “Physical activity and young people” World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.cdc.gov/physicalactivity/basics/measuring/index.html')}>
                  <Text style={{color: 'blue'}}>
                  [14]  Centres for Disease Control and Prevention, “Measuring Physical Activity Intensity” US Department for Health and Human Services 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.worldcat.org/title/promoting-physical-activity-a-guide-for-community-action/oclc/870994843')}>
                  <Text style={{color: 'blue'}}>
                  [15]  U.S. Department of Health and Human Services, Public Health Service, Centers for Disease Control and Prevention, National Center for Chronic Disease Prevention and Health Promotion, Division of Nutrition and Physical Activity. Promoting physical activity: a guide for community action. Champaign, IL: Human Kinetics, 1999.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/19346988/')}>
                  <Text style={{color: 'blue'}}>
                  [16]  P. T. Katzmarzyk, T. S. Church, C. L. Craig, and C. Bouchard, “Sitting Time and Mortality From All Causes, Cardiovascular Disease, and Cancer,” Medicine and Science in Sport and Exercise, vol. 41, no. 5, May pp. 998-1005, 2009.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://link.springer.com/article/10.1007%2Fs00125-016-4193-z')}>
                  <Text style={{color: 'blue'}}>
                  [17]  B. O. Åsvold, K. Midthjell,S.  Krokstad. S, V. Rangul, and A. Bauman, “Prolonged sitting may increase diabetes risk in physically inactive individuals: an 11 year follow-up of the HUNT Study, Norway,” Diabetologia, vol. 60, Jan pp. 830–835, 2017. 
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(16)30370-1/fulltext')}>
                  <Text style={{color: 'blue'}}>
                  [18]  U. Ekelund, J. Steene-Johannessen, W. J. Brown, M. W. Fagerland, N. Owen, K. E. Powell, A. Bauman, I. Lee, et al, “Does physical activity attenuate, or even eliminate, the detrimental association of sitting time with mortality? A harmonised meta-analysis of data from more than 1 million men and women,” The Lancet, vol. 388, no. 10051, Sep pp. 1302-1310, 2016.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.onlinejacc.org/content/73/16/2062')}>
                  <Text style={{color: 'blue'}}>
                  [19]  E. Stamatakis, J. Gale, A. Bauman, U. Ekelund, M. Hamer, and D. Ding, “Sitting Time, Physical Activity, and Risk of Mortality in Adults,” Journal of the American College of Cardiology, vol. 73, no. 16, pp. 2067-2072, 2019.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.nhs.uk/live-well/exercise/why-sitting-too-much-is-bad-for-us/')}>
                  <Text style={{color: 'blue'}}>
                  [20]  NHS, “Why we should sit less”, 2019.
                  </Text>
                </TouchableOpacity>                
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://ijbnpa.biomedcentral.com/articles/10.1186/s12966-017-0525-8')}>
                  <Text style={{color: 'blue'}}>
                  [21]  M. S. Tremblay, S. Aubert, J. D. Barnes, et al. “Sedentary Behavior Research Network (SBRN) – Terminology Consensus Project process and outcome,” International Journal of Behavioural Nutrition and Physical Activity, vol. 14, no. 75, 2017.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/dietphysicalactivity/pa/en/')}>
                  <Text style={{color: 'blue'}}>
                  [22]  World Health Organization, “Physical Activity” World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://ijbnpa.biomedcentral.com/articles/10.1186/1479-5868-8-79')}>
                  <Text style={{color: 'blue'}}>
                  [23]  C. Tudor-Locke, C. L. Craig, W. J. Brown et al., “How many steps/day are enough? for adults,” International Journal of Behavioural Nutrition and Physical Activity, vol 8, no. 79, 2011.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/news-room/campaigns/connecting-the-world-to-combat-coronavirus/healthyathome/healthyathome---physical-activity')}>
                  <Text style={{color: 'blue'}}>
                  [24]  World Health Organization, “#HealthyAtHome – Physical activity,” World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.euro.who.int/en/health-topics/health-emergencies/coronavirus-covid-19/technical-guidance/stay-physically-active-during-self-quarantine')}>
                  <Text style={{color: 'blue'}}>
                  [25]  World Health Organization, “Stay physically active during self-quarantine,” World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://theconversation.com/how-to-stay-fit-and-active-at-home-during-the-coronavirus-self-isolation-134044')}>
                  <Text style={{color: 'blue'}}>
                  [26]  E. Stamatakis, A. Murry, F. Bull, and K. Edwards, “How to stay fit and active at home during the coronavirus self-isolation
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.isbnpa.org/index.php?r=article/view&id=146')}>
                  <Text style={{color: 'blue'}}>
                  [27]  J. F. Sallis, and M. Pratt, “Physical Activity Can Be Helpful in the Coronavirus Pandemic,” The International Society for Physical Activity and Health, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.acsm.org/blog-detail/acsm-blog/2020/03/30/exercise-immunity-covid-19-pandemic')}>
                  <Text style={{color: 'blue'}}>
                  [28]  R. J. Simpson, “Exercise, Immunity and the COVID-19 pandemic,” American College of Sports Medicine, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/pii/S2095254618301005?via%3Dihub')}>
                  <Text style={{color: 'blue'}}>
                  [29]  D. C. Nieman, and L. M. Wentz, “The compelling link between physical activity and the body's defense system,” Journal of Sport and Health Science, vol. 8, no. 3, May pp. 201- 217, 2019.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.acsm.org/read-research/newsroom/news-releases/news-detail/2020/03/16/staying-physically-active-during-covid-19-pandemic')}>
                  <Text style={{color: 'blue'}}>
                  [30]  Exercise is Medicine, “Staying active during the coronavirus pandemic,” American College of Sports Medicine, 2020.
                  </Text>
                </TouchableOpacity>                
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.worldcat.org/title/exercise-physiology-nutrition-energy-and-human-performance/oclc/987653689')}>
                  <Text style={{color: 'blue'}}>
                  [31]  W. D. McArdle, F. I. Katch, and V. L. Katch, Exercise physiology: Nutrition, energy, and human performance, 7th Ed., Baltimore, MD: Lippincott Williams & Wilkins, 2010
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4139760/')}>
                  <Text style={{color: 'blue'}}>
                  [32]  American College of Sports Medicine. ACSM’s Guidelines for exercise testing and prescription. Philadelphia: Lippincott Williams & Wilkins, 2000.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2716237/')}>
                  <Text style={{color: 'blue'}}>
                  [33]  H. S. Buttar, T. Li, and N. Ravi, “Prevention of cardiovascular diseases: Role of exercise, dietary interventions, obesity and smoking cessation,” Experimental and Clinical Cardiology, vol. 10, no. 4, pp. 229-249, 2005.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://jamanetwork.com/journals/jamainternalmedicine/article-abstract/415827')}>
                  <Text style={{color: 'blue'}}>
                  [34]  E. Kvaavik, G. D. Batty, G. Ursin, et al., “Influence of individual and combined health behaviours on total and cause-specific mortality in men and women: the United Kingdom Health and Lifestyle Survey,” Archives of Internal Medicine, vol. 170, no. 8, pp.711–718, 2010.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.frontiersin.org/articles/10.3389/fphys.2017.00865/full')}>
                  <Text style={{color: 'blue'}}>
                  [35]  G. E. Vincent, S. M. Jay, C. Sargent, C. Vandelanotte, N. D. Ridgers, and S. A. Ferguson, “Improving Cardiometabolic Health with Diet, Physical Activity, and Breaking Up Sitting: What about Sleep?” Frontiers in Physiology, vol. 8, no. 865.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4341978/')}>
                  <Text style={{color: 'blue'}}>
                  [36]  C. E. Kline, “The bidirectional relationship between exercise and sleep: Implications for exercise adherence and sleep improvement,” American Journal of Lifestyle Medicine, vol. 8, no. 6, pp. 375-379, 2014.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.nhs.uk/live-well/sleep-and-tiredness/why-lack-of-sleep-is-bad-for-your-health/')}>
                  <Text style={{color: 'blue'}}>
                  [37]  NHS, “Why lack of sleep is bad for your health”, 2018.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://breathe.ersjournals.com/content/12/1/97')}>
                  <Text style={{color: 'blue'}}>
                  [38]  “Your lungs and exercise,” Breathe (Sheff), vol. 12, no. 1, Mar pp. 97-100, 2016.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.drinkaware.co.uk/facts/health-effects-of-alcohol/lifestyle/can-alcohol-affect-sports-performance-and-fitness-levels')}>
                  <Text style={{color: 'blue'}}>
                  [39]  G. Whyte, “Can alcohol affect sports performance and fitness levels,” drinkaware.com, para. 4.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.nature.com/articles/ijo2008206')}>
                  <Text style={{color: 'blue'}}>
                  [40]  P. Suter, and Y. Schutz, “The effect of exercise, alcohol or both combined on health and physical performance,” International Journal of Obesity, vol. 32, pp. S48–S52, 2008.
                  </Text>
                </TouchableOpacity>                
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/dietphysicalactivity/factsheet_olderadults/en/')}>
                  <Text style={{color: 'blue'}}>
                  [41]  World Health Organization, “Physical Activity and Older Adults” World Health Organization, 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.tandfonline.com/doi/abs/10.1080/02640410410001712421')}>
                  <Text style={{color: 'blue'}}>
                  [42]  A. H. Taylor, N. T. Cable, G. Faulkner, M. Hillsdon, M. NArici, and A. K. Van Der Bij ,”Physical activity and older adults: a review of health benefits and the effectiveness of interventions,” Journal of Sports Sciences, vol. 22, no. 8, pp. 703-725, 2004.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.nhs.uk/live-well/exercise/10-minute-workouts/')}>
                  <Text style={{color: 'blue'}}>
                  [43] NHS, “10-minute workouts”, 2018.
                  </Text>
                </TouchableOpacity>
              </View>
            </CollapsibleView>
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default Physical

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    flexWrap: 'wrap'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  content: {
    flex: 1
  },
  margin: {
    marginTop: 15,
  }
})
