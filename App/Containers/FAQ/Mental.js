
// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Linking } from 'react-native'
import ParsedText from 'react-native-parsed-text'
import Collapsible from 'react-native-collapsible'
import propTypes from 'prop-types'
import {Icon, Card} from 'react-native-elements'

// import { NavigationActions } from 'react-navigation'
import {Colors} from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

const questions = []

for (let i = 1; i <= 12; i++) {
  questions.push({
    title: `Faq-Mental.questions.${i}.title`,
    answer: `Faq-Mental.questions.${i}.answer`
  })
}

class CollapsibleView extends Component {
  static propTypes = {
    title: propTypes.string
  }
  state = {
    collapsed: true
  }

  render () {
    const {title} = this.props
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({collapsed: !this.state.collapsed})} style={{flexDirection: 'row', alignItems: 'flex-start', marginRight: 15}}>
          <Icon name={this.state.collapsed ? 'chevron-right' : 'chevron-down'} type='font-awesome' containerStyle={{marginTop: 3, width: 15, marginRight: 5}} iconStyle={[styles.headline, {fontSize: 14}]} />
          <Text style={[styles.headline, {marginRight: 15}]}>{title}</Text>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    )
  }
}

class Mental extends Component {
  state = {
    activeSection: false,
    collapsed: true
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  setSection = section => {
    this.setState({ activeSection: section })
  }

  renderNavigationbar (props) {
    let title = I18n.t('Faq-Mental.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
          <Card
            title={I18n.t('Faq-Mental.faqTitle')}
            titleStyle={styles.cardTitle}
            containerStyle={{marginBottom: 15}}
            >
            <View key={1}>
              {
                questions.map((question, i) =>
                  <CollapsibleView key={i} title={(i + 1) + '. ' + I18n.t(question.title)}>
                    <View style={{ flex: 1 }}>
                      <ParsedText
                        style={styles.paragraph}
                        parse={[{type: 'email', style: styles.url, onPress: (mail) => openURL('mailto:' + mail)},
                        {type: 'url', style: styles.url, onPress: (url) => openURL(url)}]}
                      >
                        {I18n.t(question.answer)}
                      </ParsedText>
                    </View>
                  </CollapsibleView>
                )
              }
            </View>
            <CollapsibleView title={I18n.t('Faq-Mental.questions.13.title')}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => Linking.openURL('https://www.coaching-report.de/literatur/rezensionen/rezension-details/coaching-mit-ressourcenaktivierung.html')}>
                  <Text style={{color: 'blue'}}>
                  [1]  M. Deubner-Böhme and U. Deppe-Schmitz, Coaching mit Ressourcenaktivierung: ein Leitfaden für Coaches, Berater und Trainer, 1. Auflage. Göttingen: Hogrefe, 2018.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/6342368/')}>
                  <Text style={{color: 'blue'}}>
                  [2]  W. E. Broadhead et al., “THE EPIDEMIOLOGIC EVIDENCE FOR A RELATIONSHIP BETWEEN SOCIAL SUPPORT AND HEALTH,” American Journal of Epidemiology, vol. 117, no. 5, pp. 521–537, May 1983, doi: 10.1093/oxfordjournals.aje.a113575.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://books.google.ch/books?hl=en&lr=&id=V62dDQAAQBAJ&oi=fnd&pg=PP1&dq=M.+von+Wachter+and+A.+Hendrischke,+Das+Ressourcenbuch+Selbstheilungskr%C3%A4fte+in+der+Psychotherapie+erkennen+und+von+Anfang+an+f%C3%B6rder&ots=PD1dallMQ1&sig=Npy4ckaTf42mrmnFvAXO0L0YomU')}>
                  <Text style={{color: 'blue'}}>
                  [3]  M. von Wachter and A. Hendrischke, Das Ressourcenbuch Selbstheilungskräfte in der Psychotherapie erkennen und von Anfang an fördern. 2017.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://link.springer.com/chapter/10.1007/978-94-015-8486-9_7')}>
                  <Text style={{color: 'blue'}}>
                  [4]  R. Schwarzer and M. Jerusalem, “Optimistic Self-Beliefs as a Resource Factor in Coping with Stress,” in Extreme Stress and Communities: Impact and Intervention, S. E. Hobfoll and M. W. Vries, Eds. Dordrecht: Springer Netherlands, 1995, pp. 159–177.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://journals.sagepub.com/doi/abs/10.1037/1089-2680.6.4.307')}>
                  <Text style={{color: 'blue'}}>
                  [5]  S. E. Hobfoll, “Social and Psychological Resources and Adaptation,” Review of General Psychology, vol. 6, no. 4, pp. 307–324, Dec. 2002, doi: 10.1037/1089-2680.6.4.307.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://link.springer.com/chapter/10.1007/978-3-662-47983-4_7')}>
                  <Text style={{color: 'blue'}}>
                  [6]  M. von Wachter and A. Hendrischke, Psychoedukation bei chronischen Schmerzen. Berlin, Heidelberg: Springer, 2016.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.jstor.org/stable/24987318?seq=1')}>
                  <Text style={{color: 'blue'}}>
                  [7]  D. L. Alkon, “Memory Storage and Neural Systems,” Scientific American, vol. 261, no. 1, pp. 42–50, Jul. 1989, doi: 10.1038/scientificamerican0789-42.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Linking.openURL('https://www.annualreviews.org/doi/abs/10.1146/annurev.clinpsy.3.022806.091520casa_token=y9wltAPTSLMAAAAA%3AOxNrPcfhBhh0zqy8of0R_PfYE8sQYmjCFAe4nYhqqApXcXNVG6ZCj-0R6tvl7mWiVmpsVLDRs5fyvTw')}>
                  <Text style={{color: 'blue'}}>
                  [8]  S. E. Taylor and A. L. Stanton, “Coping Resources, Coping Processes, and Mental Health,” Annual Review of Clinical Psychology, vol. 3, no. 1, pp. 377–401, Apr. 2007, doi: 10.1146/annurev.clinpsy.3.022806.091520.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://psycnet.apa.org/record/2001-06329-007')}>
                  <Text style={{color: 'blue'}}>
                  [9]  C. A. Naranjo, L. K. Tremblay, and U. E. Busto, “The role of the brain reward system in depression,” Progress in Neuro-Psychopharmacology and Biological Psychiatry, vol. 25, no. 4, pp. 781–823, May 2001, doi: 10.1016/S0278-5846(01)00156-7.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://psycnet.apa.org/record/1989-25079-001')}>
                  <Text style={{color: 'blue'}}>
                  [10]  R. A. Wise and P. P. Rompre, “Brain dopamine and reward,” Annu Rev Psychol, vol. 40, pp. 191–225, 1989, doi: 10.1146/annurev.ps.40.020189.001203.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://journals.sagepub.com/doi/full/10.1177/2055102918793552')}>
                  <Text style={{color: 'blue'}}>
                  [11]  K. White, M. S. Issac, C. Kamoun, J. Leygues, and S. Cohn, “The THRIVE model: A framework and review of internal and external predictors of coping with chronic illness,” Health Psychol Open, vol. 5, no. 2, p. 2055102918793552, Dec. 2018, doi: 10.1177/2055102918793552.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://books.google.ch/books?hl=en&lr=&id=L9nwCwAAQBAJ&oi=fnd&pg=PT7&dq=%09U.+Deppe-Schmitz+and+M.+Deubner-B%C3%B6hme,+Auf+die+Ressourcen+kommt+es+an:+Praxis+der+Ressourcenaktivierung,+1.+Auflage.+G%C3%B6ttingen:+Hogrefe,+2016.&ots=t0cN0Q5YhO&sig=J69vNNFdt5rTSSUoqKlMw82SEJU#v=onepage&q&f=false')}>
                  <Text style={{color: 'blue'}}>
                  [12]  U. Deppe-Schmitz and M. Deubner-Böhme, Auf die Ressourcen kommt es an: Praxis der Ressourcenaktivierung, 1. Auflage. Göttingen: Hogrefe, 2016.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/8681258/')}>
                  <Text style={{color: 'blue'}}>
                  [13]  M. R. Katz, G. Rodin, and G. M. Devins, “Self-Esteem and Cancer: Theory and Research,” The Canadian Journal of Psychiatry, vol. 40, no. 10, pp. 608–615, Dec. 1995, doi: 10.1177/070674379504001007.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.jstor.org/stable/2626957')}>
                  <Text style={{color: 'blue'}}>
                  [14]  P. A. Thoits, “Stress, Coping, and Social Support Processes: Where Are We? What Next?,” Journal of Health and Social Behavior, vol. 35, p. 53, 1995, doi: 10.2307/2626957.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://psycnet.apa.org/record/2004-15580-005')}>
                  <Text style={{color: 'blue'}}>
                  [15]  M. I. Bisschop, D. M. . Kriegsman, A. T. . Beekman, and D. J. . Deeg, “Chronic diseases and depression: the modifying role of psychosocial resources,” Social Science & Medicine, vol. 59, no. 4, pp. 721–733, Aug. 2004, doi: 10.1016/j.socscimed.2003.11.038.
                  </Text>
                </TouchableOpacity>
              </View>
            </CollapsibleView>
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default Mental

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    flexWrap: 'wrap'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  content: {
    flex: 1
  },
  margin: {
    marginTop: 15,
  }
})
