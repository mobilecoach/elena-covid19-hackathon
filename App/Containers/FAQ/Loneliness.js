
// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Linking } from 'react-native'
import ParsedText from 'react-native-parsed-text'
import Collapsible from 'react-native-collapsible'
import propTypes from 'prop-types'
import {Icon, Card} from 'react-native-elements'

// import { NavigationActions } from 'react-navigation'
import {Colors} from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

const questions = []

for (let i = 1; i <= 3; i++) {
  questions.push({
    title: `Faq-Loneliness.questions.${i}.title`,
    answer: `Faq-Loneliness.questions.${i}.answer`
  })
}

class CollapsibleView extends Component {
  static propTypes = {
    title: propTypes.string
  }
  state = {
    collapsed: true
  }

  render () {
    const {title} = this.props
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({collapsed: !this.state.collapsed})} style={{flexDirection: 'row', alignItems: 'flex-start', marginRight: 15}}>
          <Icon name={this.state.collapsed ? 'chevron-right' : 'chevron-down'} type='font-awesome' containerStyle={{marginTop: 3, width: 15, marginRight: 5}} iconStyle={[styles.headline, {fontSize: 14}]} />
          <Text style={[styles.headline, {marginRight: 15}]}>{title}</Text>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    )
  }
}

class Loneliness extends Component {
  state = {
    activeSection: false,
    collapsed: true
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  setSection = section => {
    this.setState({ activeSection: section })
  }

  renderNavigationbar (props) {
    let title = I18n.t('Faq-Loneliness.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
          <Card
            title={I18n.t('Faq-Loneliness.faqTitle')}
            titleStyle={styles.cardTitle}
            containerStyle={{marginBottom: 15}}
            >
            <View key={1}>
              {
                questions.map((question, i) =>
                  <CollapsibleView key={i} title={(i + 1) + '. ' + I18n.t(question.title)}>
                    <View style={{ flex: 1 }}>
                      <ParsedText
                          style={styles.paragraph}
                          parse={[{type: 'email', style: styles.url, onPress: (mail) => openURL('mailto:' + mail)},
                          {type: 'url', style: styles.url, onPress: (url) => openURL(url)}]}
                        >
                          {I18n.t(question.answer)}
                        </ParsedText>
                    </View>
                  </CollapsibleView>
                )
              }
            </View>
            <CollapsibleView title={I18n.t('Faq-Loneliness.questions.4.title')}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => Linking.openURL('https://www.worldcat.org/title/loneliness-a-sourcebook-of-current-theory-research-and-therapy/oclc/7947089')}>
                  <Text style={{color: 'blue'}}>
                  [1]  Peplau, L. A., & Perlman, D. (1982). Perspectives on loneliness. In L. A. Peplau & D. Perlman (Eds.), Loneliness: A source- book of current theory, research and therapy (pp. 1–8). New York, NY: Wiley.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2792572/')}>
                  <Text style={{color: 'blue'}}>
                  [2]  Cacioppo, J. T., Fowler, J. H., & Christakis, N. A. (2009). Alone in the crowd: The structure and spread of loneliness in a large social network. Journal of Personality and Social Psychology, 97, 977–991.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5090712/')}>
                  <Text style={{color: 'blue'}}>
                  [3]  Cacioppo, J. T., Cacioppo, S., Cole, S. W., Capitanio, J. P., Goossens, L., & Boomsma, D. I. (2015). Loneliness across phylogeny and a call for animal models. Perspectives on Psychological Science, 10, 202–212.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.wsj.com/articles/SB122773240294660733')}>
                  <Text style={{color: 'blue'}}>
                  [4]  Cacioppo, J. T., & Patrick, W. (2008). Loneliness: Human nature and the need for social connection. New York, NY: Norton.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2922929/')}>
                  <Text style={{color: 'blue'}}>
                  [5]  Cacioppo, J. T., Hawkley, L. C., & Thisted, R. A. (2010). Perceived social isolation makes me sad: Five year cross- lagged analyses of loneliness and depressive symptom- atology in the Chicago Health, Aging, and Social Relations Study. Psychology and Aging, 25, 453–463.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/16594799/')}>
                  <Text style={{color: 'blue'}}>
                  [6]  Cacioppo, J. T., Hughes, M. E., Waite, L. J., Hawkley, L. C., & Thisted, R. A. (2006). Loneliness as a specific risk factor for depressive symptoms: Cross sectional and longitudinal analyses. Psychology and Aging, 21, 140–151.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/21443322/')}>
                  <Text style={{color: 'blue'}}>
                  [7]  VanderWeele, T. J., Hawkley, L. C., Thisted, R. A., & Cacioppo, J. T. (2011). A marginal structural model analysis for loneliness: Implications for intervention trials and clinical practice. Journal of Consulting and Clinical Psychology, 79, 225–235. doi:10.1037/a0022610
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7190373/')}>
                  <Text style={{color: 'blue'}}>
                  [8]  Jacobi, F. (2020) Häusliche Isolation und Quarantäne gut überstehen. Psychotherapeut, 1.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://psycnet.apa.org/record/2005-08033-003')}>
                  <Text style={{color: 'blue'}}>
                  [9]  Seligman, M. E., Steen, T. A., Park, N., & Peterson, C. (2005). Positive psychology progress: empirical validation of interventions. American psychologist, 60(5), 410.
                  </Text>
                </TouchableOpacity>
              </View>
            </CollapsibleView>
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default Loneliness

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    flexWrap: 'wrap'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  content: {
    flex: 1
  },
  margin: {
    marginTop: 15,
  }
})
