
// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Linking } from 'react-native'
import ParsedText from 'react-native-parsed-text'
import Collapsible from 'react-native-collapsible'
import propTypes from 'prop-types'
import {Icon, Card} from 'react-native-elements'

// import { NavigationActions } from 'react-navigation'
import {Colors} from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

const questions = []

for (let i = 1; i <= 5; i++) {
  questions.push({
    title: `Faq-Anxiety.questions.${i}.title`,
    answer: `Faq-Anxiety.questions.${i}.answer`
  })
}

class CollapsibleView extends Component {
  static propTypes = {
    title: propTypes.string
  }
  state = {
    collapsed: true
  }

  render () {
    const {title} = this.props
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({collapsed: !this.state.collapsed})} style={{flexDirection: 'row', alignItems: 'flex-start', marginRight: 15}}>
          <Icon name={this.state.collapsed ? 'chevron-right' : 'chevron-down'} type='font-awesome' containerStyle={{marginTop: 3, width: 15, marginRight: 5}} iconStyle={[styles.headline, {fontSize: 14}]} />
          <Text style={[styles.headline, {marginRight: 15}]}>{title}</Text>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    )
  }
}

class Anxiety extends Component {
  state = {
    activeSection: false,
    collapsed: true
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  setSection = section => {
    this.setState({ activeSection: section })
  }

  renderNavigationbar (props) {
    let title = I18n.t('Faq-Anxiety.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
          <Card
            title={I18n.t('Faq-Anxiety.faqTitle')}
            titleStyle={styles.cardTitle}
            containerStyle={{marginBottom: 15}}
            >
            <View key={1}>
              {
                questions.map((question, i) =>
                  <CollapsibleView key={i} title={(i + 1) + '. ' + I18n.t(question.title)}>
                    <View style={{ flex: 1 }}>
                      <ParsedText
                        style={styles.paragraph}
                        parse={[{type: 'email', style: styles.url, onPress: (mail) => openURL('mailto:' + mail)},
                        {type: 'url', style: styles.url, onPress: (url) => openURL(url)}]}
                      >
                        {I18n.t(question.answer)}
                      </ParsedText>
                    </View>
                  </CollapsibleView>
                )
              }
            </View>
            <CollapsibleView title={I18n.t('Faq-Anxiety.questions.6.title')}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => Linking.openURL('https://www.taylorfrancis.com/books/e/9780203728215')}>
                  <Text style={{color: 'blue'}}>
                  [1]  Tuma AH, Maser JD. Anxiety and the anxiety disorders: Routledge; 2019.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://asprtracie.hhs.gov/technical-resources/resource/8272/coping-with-fear-and-sadness-during-a-pandemic')}>
                  <Text style={{color: 'blue'}}>
                  [2]  Sanderson WC. Coping with Fear and Sadness During a Pandemic. 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/docs/default-source/coronaviruse/mental-health-considerations.pdf?sfvrsn=6d3578af_2')}>
                  <Text style={{color: 'blue'}}>
                  [3]  World Health Organization. Mental health and psychosocial considerations during the COVID-19 outbreak, 18 March 2020. World Health Organization; 2020.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://researchers.mq.edu.au/en/publications/managing-anxiety-disorders-in-adult')}>
                  <Text style={{color: 'blue'}}>
                  [4]  Wilkins G, Andrews G, Bell C, Boyce P, Gale C, Rapee R, et al. Managing anxiety disorders in adults. Medicine Today. 2019;20(12):12-22.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/abs/pii/S1077722912000818')}>
                  <Text style={{color: 'blue'}}>
                  [5]  Erickson TM, Scarsella G. Clark, DA, & Beck, AT (2012) The Anxiety and Worry Workbook: The Cognitive Behavioral Solution New York: The Guilford Press. Elsevier; 2013.
                  </Text>
                </TouchableOpacity>
              </View>
            </CollapsibleView>
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default Anxiety

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    flexWrap: 'wrap'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  content: {
    flex: 1
  },
  margin: {
    marginTop: 15,
  }
})
