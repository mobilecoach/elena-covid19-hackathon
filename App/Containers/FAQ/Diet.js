
// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Linking } from 'react-native'
import ParsedText from 'react-native-parsed-text'
import Collapsible from 'react-native-collapsible'
import propTypes from 'prop-types'
import {Icon, Card} from 'react-native-elements'

// import { NavigationActions } from 'react-navigation'
import {Colors} from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

const questions = []

for (let i = 1; i <= 9; i++) {
  questions.push({
    title: `Faq-Diet.questions.${i}.title`,
    answer: `Faq-Diet.questions.${i}.answer`
  })
}

class CollapsibleView extends Component {
  static propTypes = {
    title: propTypes.string
  }
  state = {
    collapsed: true
  }

  render () {
    const {title} = this.props
    return (
      <View>
        <TouchableOpacity onPress={() => this.setState({collapsed: !this.state.collapsed})} style={{flexDirection: 'row', alignItems: 'flex-start', marginRight: 15}}>
          <Icon name={this.state.collapsed ? 'chevron-right' : 'chevron-down'} type='font-awesome' containerStyle={{marginTop: 3, width: 15, marginRight: 5}} iconStyle={[styles.headline, {fontSize: 14}]} />
          <Text style={[styles.headline, {marginRight: 15}]}>{title}</Text>
        </TouchableOpacity>
        <Collapsible collapsed={this.state.collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    )
  }
}

class Diet extends Component {
  state = {
    activeSection: false,
    collapsed: true
  }

  toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed })
  }

  setSection = section => {
    this.setState({ activeSection: section })
  }

  renderNavigationbar (props) {
    let title = I18n.t('Faq-Diet.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
          <Card
            title={I18n.t('Faq-Diet.faqTitle')}
            titleStyle={styles.cardTitle}
            containerStyle={{marginBottom: 15}}
            >
            <View key={1}>
              {
                questions.map((question, i) =>
                  <CollapsibleView key={i} title={(i + 1) + '. ' + I18n.t(question.title)}>
                    <View style={{ flex: 1 }}>
                      <ParsedText
                          style={styles.paragraph}
                          parse={[{type: 'email', style: styles.url, onPress: (mail) => openURL('mailto:' + mail)},
                          {type: 'url', style: styles.url, onPress: (url) => openURL(url)}]}
                        >
                          {I18n.t(question.answer)}
                        </ParsedText>
                    </View>
                  </CollapsibleView>
                )
              }
            </View>
            <CollapsibleView title={I18n.t('Faq-Diet.questions.10.title')}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/30744710/')}>
                  <Text style={{color: 'blue'}}>
                  [1]  C. A. Monteiro et al., “Ultra-processed foods: what they are and how to identify them,” Public Health Nutr., vol. 22, no. 5, pp. 936–941, 2019.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.1001235')}>
                  <Text style={{color: 'blue'}}>
                  [2]  D. Stuckler, M. McKee, S. Ebrahim, and S. Basu, “Manufacturing Epidemics: The Role of Global Producers in Increased Consumption of Unhealthy Commodities Including Processed Foods, Alcohol, and Tobacco,” PLoS Med., vol. 9, no. 6, p. e1001235, Jun. 2012, doi: 10.1371/journal.pmed.1001235.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.cambridge.org/core/journals/public-health-nutrition/article/household-availability-of-ultraprocessed-foods-and-obesity-in-nineteen-european-countries/D63EF7095E8EFE72BD825AFC2F331149')}>
                  <Text style={{color: 'blue'}}>
                  [3]  C. A. Monteiro, J. C. Moubarac, R. B. Levy, D. S. Canella, M. L. Da Costa Louzada, and G. Cannon, “Household availability of ultra-processed foods and obesity in nineteen European countries,” Public Health Nutr., vol. 21, no. 1, pp. 18–26, Jan. 2018, doi: 10.1017/S1368980017001379.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://journals.sagepub.com/doi/abs/10.1177/1741826711425777')}>
                  <Text style={{color: 'blue'}}>
                  [4]  E. Dunford et al., “International collaborative project to compare and monitor the nutritional composition of processed foods,” Eur. J. Prev. Cardiol., vol. 19, no. 6, pp. 1326–1332, Dec. 2012, doi: 10.1177/1741826711425777.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://onlinelibrary.wiley.com/doi/10.1111/obr.12174')}>
                  <Text style={{color: 'blue'}}>
                  [5]  P. Baker and S. Friel, “Processed foods and the nutrition transition: Evidence from Asia,” Obes. Rev., vol. 15, no. 7, pp. 564–577, Jul. 2014, doi: 10.1111/obr.12174.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/26270019/')}>
                  <Text style={{color: 'blue'}}>
                  [6]  M. L. da C. Louzada et al., “Impact of ultra-processed foods on micronutrient content in the Brazilian diet,” Rev. Saude Publica, vol. 49, no. 0, pp. 1–8, Aug. 2015, doi: 10.1590/S0034-8910.2015049006211.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.who.int/mediacentre/news/statements/2015/processed-meat-cancer/en/')}>
                  <Text style={{color: 'blue'}}>
                  [7]  World Health Organization, “Links between processed meat and colorectal cancer.” 2015.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/21160428/')}>
                  <Text style={{color: 'blue'}}>
                  [8]  J. Hu, C. La Vecchia, H. Morrison, E. Negri, and L. Mery, “Salt, processed meat and the risk of cancer,” Eur. J. Cancer Prev., vol. 20, no. 2, pp. 132–139, Mar. 2011, doi: 10.1097/CEJ.0b013e3283429e32.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/pii/S0278691516301144')}>
                  <Text style={{color: 'blue'}}>
                  [9]  L. D. Boada, L. A. Henríquez-Hernández, and O. P. Luzardo, “The impact of red and processed meat consumption on cancer and other health outcomes: Epidemiological evidences,” Food and Chemical Toxicology, vol. 92. Elsevier Ltd, pp. 236–244, 01-Jun-2016, doi: 10.1016/j.fct.2016.04.008.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://journals.plos.org/plosmedicine/article?id=10.1371/journal.pmed.0040325')}>
                  <Text style={{color: 'blue'}}>
                  [10]  A. J. Cross, M. F. Leitzmann, M. H. Gail, A. R. Hollenbeck, A. Schatzkin, and R. Sinha, “A Prospective Study of Red and Processed Meat Intake in Relation to Cancer Risk,” PLoS Med., vol. 4, no. 12, p. e325, Dec. 2007, doi: 10.1371/journal.pmed.0040325.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://apps.who.int/iris/bitstream/handle/10665/325828/EMROPUB_2019_en_23536.pdf')}>
                  <Text style={{color: 'blue'}}>
                  [11]  World Health Organization, “Healthy diet,” World Health Organization. Regional Office for the Eastern Mediterranean, 2019.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.cambridge.org/core/journals/public-health-nutrition/article/nutrition-and-health-the-issue-is-not-food-nor-nutrients-so-much-as-processing/0C514FC9DB264538F83D5D34A81BB10A')}>
                  <Text style={{color: 'blue'}}>
                  [12]  C. A. Monteiro, “Nutrition and health. The issue is not food, nor nutrients, so much as processing,” Public Health Nutrition, vol. 12, no. 5. Cambridge University Press, pp. 729–731, May-2009, doi: 10.1017/S1368980009005291.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://link.springer.com/article/10.1007/s11524-010-9453-5')}>
                  <Text style={{color: 'blue'}}>
                  [13]  S. C. Lucan, A. Karpyn, and S. Sherman, “Storing Empty Calories and Chronic Disease Risk: Snack-Food Products, Nutritive Content, and Manufacturers in Philadelphia Corner Stores,” J. Urban Heal. Bull. New York Acad. Med., vol. 87, no. 3, doi: 10.1007/s11524-010-9453-5.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/9863851/')}>
                  <Text style={{color: 'blue'}}>
                  [14]  D. Ornish et al., “Intensive lifestyle changes for reversal of coronary heart disease,” J. Am. Med. Assoc., vol. 280, no. 23, pp. 2001–2007, 1998, doi: 10.1001/jama.280.23.2001.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://academic.oup.com/ajcn/article/89/5/1588S/4596944')}>
                  <Text style={{color: 'blue'}}>
                  [15]  N. D. Barnard et al., “A low-fat vegan diet and a conventional diabetes diet in the treatment of type 2 diabetes: a randomized, controlled, 74-wk clinical trial,” Am. J. Clin. Nutr., vol. 89, no. 5, pp. 1588S-1596S, May 2009, doi: 10.3945/ajcn.2009.26736H.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://link.springer.com/article/10.1007/s11892-010-0093-7')}>
                  <Text style={{color: 'blue'}}>
                  [16]  C. B. Trapp and N. D. Barnard, “Usefulness of Vegetarian and Vegan Diets for Treating Type 2 Diabetes,” doi: 10.1007/s11892-010-0093-7.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://pubmed.ncbi.nlm.nih.gov/25311617/')}>
                  <Text style={{color: 'blue'}}>
                  [17]  J. McDougall et al., “Effects of 7 days on an ad libitum low-fat vegan diet: The McDougall Program cohort,” Nutr. J., vol. 13, no. 1, pp. 1–7, Oct. 2014, doi: 10.1186/1475-2891-13-99.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://jandonline.org/article/S2212-2672(13)00122-6/abstract')}>
                  <Text style={{color: 'blue'}}>
                  [18]  S. M. Rink et al., “Self-Report of fruit and vegetable intake that meets the 5 a day recommendation is associated with reduced levels of oxidative stress biomarkers and increased levels of antioxidant defense in premenopausal women,” J. Acad. Nutr. Diet., vol. 113, no. 6, pp. 776–785, Jun. 2013, doi: 10.1016/j.jand.2013.01.019.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.sciencedirect.com/science/article/abs/pii/S0002822306013794')}>
                  <Text style={{color: 'blue'}}>
                  [19]  P. M. Guenther, K. W. Dodd, J. Reedy, and S. M. Krebs-Smith, Most Americans Eat Much Less than Recommended Amounts of Fruits and Vegetables,” J. Am. Diet. Assoc., vol. 106, no. 9, pp. 1371–1379, Sep. 2006, doi: 10.1016/j.jada.2006.06.002.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://www.worldcat.org/title/how-not-to-die-discover-the-foods-scientifically-proven-to-prevent-and-reverse-disease/oclc/927104174')}>
                  <Text style={{color: 'blue'}}>
                  [20]  M. Greger and G. Stone, How not to die: discover the foods scientifically proven to prevent and reverse disease. Pan Macmillan, 2016.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://jissn.biomedcentral.com/articles/10.1186/s12970-017-0192-9/')}>
                  <Text style={{color: 'blue'}}>
                  [21]  D. Rogerson, “Vegan diets: Practical advice for athletes and exercisers,” Journal of the International Society of Sports Nutrition, vol. 14, no. 1. BioMed Central Ltd., pp. 1–15, 13-Sep-2017, doi: 10.1186/s12970-017-0192-9.
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.margin} onPress={() => Linking.openURL('https://academic.oup.com/ajcn/article-abstract/48/3/852/4716470?redirectedFrom=fulltext')}>
                  <Text style={{color: 'blue'}}>
                  [22]  V. Herbert, “Vitamin B-12: plant sources, requirements, and assay | The American Journal of Clinical Nutrition | Oxford Academic,” 1988.
                  </Text>
                </TouchableOpacity>
              </View>
            </CollapsibleView>
          </Card>
        </ScrollView>
      </View>
    )
  }
}

export default Diet

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
    flexWrap: 'wrap'
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  content: {
    flex: 1
  },
  margin: {
    marginTop: 15,
  }
})
