// TODO for improvement check: https://github.com/idibidiart/react-native-responsive-grid/blob/master/UniversalTiles.md

import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  Platform,
} from 'react-native'
import ParsedText from 'react-native-parsed-text'
import { connect } from 'react-redux'
import KeyboardSpacer from 'react-native-keyboard-spacer'

// import { NavigationActions } from 'react-navigation'
import { Images, Colors } from '../../Themes/'
import PMNavigationBar from '../../Components/Navbar'
import I18n from '../../I18n/I18n'
import { Card } from 'react-native-elements'
import ServerMessageActions from '../../Redux/MessageRedux'
import ResponsiveImage from '../../Components/ResponsiveImage'
// import SendDebugStateButton from './SendDebugStateButton'

import Log from '../../Utils/Log'
const log = new Log('Containers/Settings/Settings')

class Settings extends Component {
  renderNavigationbar (props) {
    let title = I18n.t('Settings.header')
    return (
      <PMNavigationBar title={title} props={props} rightButton={<View />} />
    )
  }

  render () {
    const { openURL } = this.props.screenProps
    const { participantId } = this.props

    return (
      <View style={styles.container}>
        {this.renderNavigationbar(this.props)}
        <ScrollView style={styles.content} indicatorStyle='white'>
        <Card
            title={I18n.t('Settings.aboutTitle')}
            titleStyle={styles.cardTitle}
          >
            <View key={1}>
              {/* Use ParsedText to open containing URLS */}
              <ParsedText
                style={styles.paragraph}
                parse={[
                  {
                    type: 'url',
                    style: styles.url,
                    onPress: openURL
                  },
                  {
                    type: 'email',
                    style: styles.url,
                    onPress: (email) => openURL('mailto:' + email)
                  }
                ]}
              >
                {I18n.t('Settings.about.copytext1')}
              </ParsedText>
            </View>
          </Card>

          <Card
            title={I18n.t('Settings.impressumTitle')}
            titleStyle={styles.cardTitle}
          >
            <View key={1}>
              {/* Use ParsedText to open containing URLS */}
              <ParsedText
                style={styles.paragraph}
                parse={[
                  {
                    type: 'url',
                    style: styles.url,
                    onPress: openURL
                  },
                  {
                    type: 'email',
                    style: styles.url,
                    onPress: (email) => openURL('mailto:' + email)
                  }
                ]}
              >
                {I18n.t('Settings.impressum.copytext1')}
              </ParsedText>
              <Text style={styles.headline}>
                {I18n.t('Settings.impressum.title2')}
              </Text>
              <ParsedText
                style={styles.paragraph}
                parse={[
                  {
                    type: 'email',
                    style: styles.url,
                    onPress: (mail) => openURL('mailto:' + mail)
                  }
                ]}
              >
                {I18n.t('Settings.impressum.copytext2')}
              </ParsedText>
              <Text style={styles.headline}>
                {I18n.t('Settings.impressum.title3')}
              </Text>
              <ParsedText
                style={styles.paragraph}
                parse={[
                  {
                    type: 'email',
                    style: styles.url,
                    onPress: (mail) => openURL('mailto:' + mail)
                  }
                ]}
              >
                {I18n.t('Settings.impressum.copytext3')}
              </ParsedText>
              <Text style={styles.headline}>
                {I18n.t('Settings.impressum.title4')}
              </Text>
              <Text style={styles.paragraph}>
                {I18n.t('Settings.impressum.copytext4')}
              </Text>
              <Text style={styles.headline}>
                {I18n.t('Settings.impressum.title5')}
              </Text>
              {/* Use ParsedText to open containing URLS */}
              <ParsedText
                style={styles.paragraph}
                parse={[
                  {
                    type: 'url',
                    style: styles.url,
                    onPress: openURL
                  },
                  {
                    type: 'email',
                    style: styles.url,
                    onPress: (mail) => openURL('mailto:' + mail)
                  }
                ]}
              >
                {I18n.t('Settings.impressum.copytext5')}
              </ParsedText>
              <Text style={styles.headline}>
                {I18n.t('Settings.impressum.title6')}
              </Text>
              {/* Use ParsedText to open containing URLS */}
              <ParsedText
                style={styles.paragraph}
                parse={[
                  {
                    type: 'url',
                    style: styles.url,
                    onPress: openURL
                  },
                  {
                    type: 'email',
                    style: styles.url,
                    onPress: (mail) => openURL('mailto:' + mail)
                  }
                ]}
              >
                {I18n.t('Settings.impressum.copytext6')}
              </ParsedText>
              <Text style={styles.participant}>{participantId}</Text>
            </View>
          </Card>
          <Card>
            <ResponsiveImage
              style={{
                alignSelf: 'center',
                width: 50,
              }}
              source={Images.poweredByLogo}
            />
            <ParsedText
              style={styles.textMiddle}
              parse={[
                {
                  type: 'url',
                  style: styles.url,
                  onPress: openURL
                }
              ]}
            >
              https://www.mobile-coach.eu
            </ParsedText>
          </Card>
        </ScrollView>
        {Platform.OS === 'ios' ? <KeyboardSpacer /> : null}
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    participantId: state.serverSyncSettings.participantId,
  }
}

const mapStateToDispatch = (dispatch) => ({
  sendFeedback: (content) =>
    dispatch(
      ServerMessageActions.sendIntention(null, 'send-app-feedback', content)
    )
})

export default connect(
  mapStateToProps,
  mapStateToDispatch
)(Settings)

const styles = StyleSheet.create({
  url: {
    color: Colors.buttons.common.background
  },
  headline: {
    color: Colors.main.headline,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10
  },
  // paragraph: {
  //   fontSize: Fonts.size.small,
  //   color: Colors.main.paragraph
  // },
  textMiddle: {
    textAlign: 'center',
    marginTop: 20,
    paddingBottom: 30,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.main.appBackground,
    borderRadius: 10
  },
  cardTitle: {
    textAlign: 'left',
    color: Colors.main.headline
  },
  cardText: {
    color: Colors.main.paragraph
  },
  content: {
    flex: 1
  },
  button: {
    backgroundColor: Colors.buttons.common.background,
    borderRadius: 20,
    marginVertical: 10
  },
  buttonText: { color: Colors.buttons.common.text, fontSize: 16 },
  participant: {
    marginTop: 10,
    fontWeight: "bold",
  }
})
