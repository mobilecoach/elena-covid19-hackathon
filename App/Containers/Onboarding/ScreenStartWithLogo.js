import React, { Component } from 'react'
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { Text, View, Image, StyleSheet, Platform, ImageBackground } from 'react-native'
import { connect } from 'react-redux'
import SplashScreen from 'react-native-splash-screen'

import NextButton from '../../Components/NextButton'
import { Colors, Images } from '../../Themes/'
import { normalize } from '../../Utils/Common'
import I18n from '../../I18n/I18n'
import MessageActions from '../../Redux/MessageRedux'
import SettingsActions from '../../Redux/SettingsRedux'

// Adjust to the appropriate next screen
const nextScreen = 'ScreenCoachSelection'

class ScreenStartWithLogo extends Component {
  componentDidMount () {
    SplashScreen.hide()
  }

  render () {
    const { sendPlatformIntention } = this.props
    const { changeLanguage } = this.props
    const { sendLanguageIntention } = this.props
    const { navigate } = this.props.navigation
    return (
      /*<View style={styles.container}>
        <View style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <Image style={styles.logoImage} source={Images.appLogo} />
          </View>
          <View style={styles.poweredByContainer}>
            <Image
              style={styles.poweredByImage}
              source={Images.poweredByLogo}
            />
          </View>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{I18n.t('Onboarding.title')}</Text>
          <Text style={styles.subtitle}>{I18n.t('Onboarding.subtitle')}</Text>
          <NextButton
            text={I18n.t('Onboarding.next')}
            onPress={() => {
              sendPlatformIntention(Platform.OS)
              changeLanguage('en-GB')
              sendLanguageIntention('en-GB')
              navigate(nextScreen)
            }}
          />
        </View>
      </View> */

      <ImageBackground source={Images.background} style={{width: '100%', height: '100%'}}>
        <View style={styles.imageContainer}>
          <View style={styles.logoContainer}>
            <Image style={styles.logoImage} source={Images.splashLogo} />
          </View>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.title}>{I18n.t('Onboarding.title')}</Text>
          <Text style={styles.subtitle}>{I18n.t('Onboarding.subtitle')}</Text>
          <NextButton
            text={I18n.t('Onboarding.next')}
            onPress={() => {
              sendPlatformIntention(Platform.OS)
              changeLanguage('en-GB')
              sendLanguageIntention('en-GB')
              navigate(nextScreen)
            }}
          />
        </View>
      </ImageBackground>
    )
  }
}

const mapStateToDispatch = (dispatch) => ({
  sendPlatformIntention: (platform) =>
    dispatch(MessageActions.sendIntention(null, 'platform', platform)),
  changeLanguage: (newLang) =>
    dispatch(SettingsActions.changeLanguage(newLang)),
  sendLanguageIntention: (language) =>
    dispatch(MessageActions.sendIntention(null, 'language', language))
})

export default connect(
  null,
  mapStateToDispatch
)(ScreenStartWithLogo)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: Colors.onboarding.background
  },
  imageContainer: {
    flex: 5,
    alignSelf: 'stretch',
    padding: 20,
    ...ifIphoneX({ paddingTop: 40 })
  },
  logoContainer: { flex: 1, flexDirection: 'row', alignItems: 'center', },
  poweredByContainer: { height: 40, alignItems: 'center' },
  logoImage: { flex: 1, resizeMode: 'contain', height: '50%'},
  poweredByImage: { flex: 1, resizeMode: 'contain' },
  textContainer: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginHorizontal: 40,
    alignSelf: 'stretch',
    marginBottom: 20
  },
  title: {
    fontSize: normalize(25),
    color: Colors.onboarding.text,
    textAlign: 'center',
    fontFamily: 'Arial'
  },
  subtitle: {
    color: Colors.onboarding.text,
    textAlign: 'center',
    fontSize: normalize(18)
  }
})
