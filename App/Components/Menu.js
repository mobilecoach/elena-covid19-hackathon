import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import { List, ListItem } from 'react-native-elements'
import { ifIphoneX } from 'react-native-iphone-x-helper'

import ResponsiveImage from './ResponsiveImage'
import I18n from '../I18n/I18n'
import Icon from 'react-native-vector-icons/Ionicons'
import ServerMessageActions from '../Redux/MessageRedux'
import StoryProgressActions from '../Redux/StoryProgressRedux'
import { Images, Colors, Metrics } from '../Themes/'
import { badgeStyles } from './Badge'

import Log from '../Utils/Log'
import { ScrollView } from 'react-native-gesture-handler'
const log = new Log('Components/Menu')

const decapitalizeFirstLetter = function (string) {
  return string.charAt(0).toLowerCase() + string.slice(1)
}

class Menu extends Component {
  constructor (props) {
    super(props)
    this.screens = {
      chat: {
        name: 'Chat',
        label: 'Menu.Chat',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-chatbubbles' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        modal: false,
        badge: () => this.getChatBadge()
      },
      covid: {
        name: 'Covid',
        label: 'Menu.Covid',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-help-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      },
      physcial: {
        name: 'Physical',
        label: 'Menu.Physical',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-help-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      },
      diet: {
        name: 'Diet',
        label: 'Menu.Diet',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-help-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      },
      mental: {
        name: 'Mental',
        label: 'Menu.Mental',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-help-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      },
      loneliness: {
        name: 'Loneliness',
        label: 'Menu.Loneliness',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-help-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      },
      anxiety: {
        name: 'Anxiety',
        label: 'Menu.Anxiety',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-help-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      },
      sleep: {
        name: 'Sleep',
        label: 'Menu.Sleep',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-help-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      },
      settings: {
        name: 'Settings',
        label: 'Menu.Settings',
        leftIcon: (
          <View style={styles.circle}>
            <Icon name='ios-information-circle' type='ionicon' style={styles.actionButtonIcon} />
          </View>
        ),
        subtitle: '',
        modal: false
      }
    }
  }

  getChatBadge () {
    if (this.props.unreadMessages === 0) return null
    else {
      let value = this.props.unreadMessages
      if (value > 99) value = '99+'
      return {
        value,
        textStyle: badgeStyles.textStyle,
        containerStyle: badgeStyles.containerStyle
      }
    }
  }

  // remember the screens visited before returning to chat
  storeVisitedScreen (screen) {
    if (!this.state.visitedScreens.includes(screen)) {
      this.setState({
        visitedScreens: [...this.state.visitedScreens, screen]
      })
    }
  }

  getList () {
    const { storyProgress } = this.props
    let list = [this.screens.chat]
    list.push(this.screens.covid)
    list.push(this.screens.physcial)
    list.push(this.screens.diet)
    list.push(this.screens.mental)
    list.push(this.screens.loneliness)
    list.push(this.screens.anxiety)
    list.push(this.screens.sleep)
    list.push(this.screens.settings)
    return list
  }

  onPressHandler (screen) {
    log.action('GUI', 'ScreenChange', screen.name)
    // Store the screen in visited screens
    this.props.onItemSelected({
      screen: screen.name,
      modal: screen.modal,
      navigationOptions: screen.navigationOptions
    })
  }

  getTitle (label) {
    let title = I18n.t(label)
    const { coach } = this.props
    if (label === 'Menu.Chat') {
      title = I18n.t(label, { coach: I18n.t('Coaches.' + coach) })
    }
    return title
  }

  renderListItem (l, i) {
    return (
      <ListItem
        // roundAvatar
        onPress={() => this.onPressHandler(l)}
        leftIcon={l.leftIcon}
        key={i}
        title={this.getTitle(l.label)}
        // subtitle={l.subtitle}
        hideChevron
        badge={l.badge ? l.badge() : null}
        titleStyle={{ color: Colors.sideMenu.text }}
      />
    )
  }

  render () {
    let imageWidth = Math.min((2 / 3) * Metrics.screenWidth - 50, 300) // sidemenu is 2/3 of screen
    return (
      <ScrollView style={styles.container}>
        <View style={{ flex: 1 }}>
          <View
            style={{
              alignItems: 'center',
              marginTop: 30,
              marginBottom: 25,
              ...ifIphoneX({ paddingTop: 20 })
            }}
          >
            <ResponsiveImage
              style={{ alignSelf: 'center', zIndex: 100 }}
              width={imageWidth}
              source={Images.sidemenuLogo}
            />
          </View>
          <View containerStyle={styles.listContainer}>
              {this.getList().map(this.renderListItem.bind(this))}
          </View>
        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    language: state.settings.language,
    storyProgress: state.storyProgress,
    coach: state.settings.coach,
    unreadMessages: state.guistate.unreadMessages,
    unreadDashboardMessages: state.storyProgress.unreadDashboardMessages
  }
}

const mapStateToDispatch = (dispatch) => ({
  sendIntention: (text, intention, content) =>
    dispatch(ServerMessageActions.sendIntention(text, intention, content)),
  resetVisitedScreens: () =>
    dispatch(StoryProgressActions.resetVisitedScreens()),
  visitScreen: (visitedScreen) =>
    dispatch(StoryProgressActions.visitScreen(visitedScreen))
})

export default connect(
  mapStateToProps,
  mapStateToDispatch
)(Menu)

Menu.propTypes = {
  onItemSelected: PropTypes.func.isRequired
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRightColor: '#bbb',
    borderRightWidth: 1,
    backgroundColor: Colors.sideMenu.background
  },
  listContainer: {
    marginBottom: 20,
    marginTop: 0,
    backgroundColor: Colors.sideMenu.buttonBackground
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 25,
    color: '#3EB076'
  },
  circle: {
  },
  image: { flex: 1, alignSelf: 'stretch', resizeMode: 'contain' }
})
