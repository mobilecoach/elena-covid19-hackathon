import Brand from './Brand'
// leave off @2x/@3x
// Use thrifty, these images are held in memory (require)!
const images = {
  appLogo: Brand.images.logo,
  poweredByLogo: Brand.images.poweredBy,
  coaches: [
    require('../Images/Coaches/avatar_elena_v2.png'),
    require('../Images/Coaches/avatar_elliot_v2.png')
  ],
  coachGeneric: require('../Images/Coaches/coach_generic.png'),
  chatBg: Brand.images.chatBackground,
  welcomeQr: require('./../Images/Onboarding/welcomeQR.png'),
  icons: {
    robot: require('../Images/Coaches/coach_generic.png')
  },
  misc: {
    panelToggle: require('./../Images/Misc/paneltoggle.png')
  },
  background: require('../Images/background_splash.png'),
  splashLogo: require('../Images/splash_logo.png'),
  sidemenuLogo: require('../Images/sidemenu_logo.png')
}

export default images
